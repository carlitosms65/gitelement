import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement {
  
  static get properties() {
    return {
			name: {type: String},			
      yearsInCompany: {type: Number},
      personInfo: {type: String},				
			photo: {type: Object}	
    };
    
    this.updatePersonInfo();
	}

  constructor() {
    super();
    
    this.name = "Prueba nombre";
    this.yearsInCompany = "12"
//    ;this.photo = {
//			src: "./img/persona.jpg",
//			alt: "foto persona"			
//		};	
    if (this.yearsInCompany >= 7) {
      this.personInfo = "lead";
     } else if (this.yearsInCompany >= 5) {
      this.personInfo = "senior";
    }	 else if (this.yearsInCompany >= 3) {
      this.personInfo = "team";
    }	 else {
      this.personInfo = "junior";
    }	
	}

  updated(changeProperties) {
    changeProperties.forEach((oldValue, propName) => {
      console.log("Propiedad " + propName + "cambia valor, anterior era " + oldValue);
    });

      if (changedProperties.has("name")) {
        console.log("Propiedad name cambiada anterior era " + changedProperties.get("name") + " nuevo es " + this.name);
      }
	}

  render() {
    return html`
      <div>
        <label>Nombre Completo</label>
        <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
        <br />						
        <label>Años en la empresa</label>
        <input type="text"  value="${this.yearsInCompany}"></input>
        <br />
        <input type="text"  value="${this.yearsInCompany}"></input>
        <br />			
      </div>
      `;
  }
  updateName(e) {
    console.log("updateName");
    console.log("Actualizando propiedad nombre con el valor " + e.target.oldValue);
    this.name=e.target.oldValue;
  }
  updateYearsInCompany(e) {
    console.log("updateYearsInCompany");
    console.log("Actualizando propiedad yearsInCompany con el valor " + e.target.oldValue);
    this.yearsInCompany=e.target.oldValue;
  }
}
customElements.define('ficha-persona', FichaPersona)