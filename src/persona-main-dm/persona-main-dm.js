import { LitElement, html } from 'lit-element';

class PersonaMainDM extends LitElement {
  
  static get properties() {
      return {
         people: {type: Array}
      }
  }

  constructor() {
      super();

      this.people = [
        {
          name: "Carlos Moreno",
          yearsInCompany: 10,
          profile: "Lorem ipsum dolor sit amet. 1",
          photo: {
              src: "./img/fotocarlos.jpg",
              alt: "Carlos Moreno"
          }
        }, {
          name: "Rodolfo Valentino",
          yearsInCompany: 2,
          profile: "Lorem ipsum dolor sit amet. 2 ffasfojfosfjooretoqfo9qh9qwenosdjnf9hfgpanfodnfjsdaofnaosdnfoasdfn9dspufnsdoñfndsofsd9pfndfnsnf",
          photo: {
              src: "./img/fotocarlos.jpg",
              alt: "Rodolfo Valentino"
          }
        }, {
          name: "Rafa Nadal",
          yearsInCompany: 5,
          profile: "Lorem ipsum dolor sit amet. 3",
          photo: {
              src: "./img/fotocarlos.jpg",
              alt: "Rafa Nadal"
          }
        }, {
          name: "Pepito Grillo",
          yearsInCompany: 1,
          profile: "Lorem ipsum dolor sit amet. 4",
          photo: {
              src: "./img/fotocarlos.jpg",
              alt: "Pepito Grillo"
          }
        },{
          name: "Florinda Chico",
          yearsInCompany: 7,
          profile: "Lorem ipsum dolor sit amet. 5",
          photo: {
              src: "./img/fotocarlos.jpg",
             alt: "Florinda Chico"
          }
        }
      ];
  }

  updated(changedProperties) {
      console.log("updated en persona-main-dm");

      if (changedProperties.has("people")) {
          console.log("ha cambiado el valor de la propiedad people en persona-main-dm");

          this.dispatchEvent(
              new CustomEvent("people-data-updated", 
                  {
                      detail: {
                          people: this.people

                      }
                  }
              )
          )
      }
  }

}

customElements.define('persona-main-dm', PersonaMainDM);