import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-main-dm/persona-main-dm.js';

class PersonaMain extends LitElement {
  
  static get properties() {
    return {
      people: {type: Array},
      showPersonForm: {type: Boolean},
      maxYearsInCompanyFilter: {type: Number}
    };
	}

  constructor() {
    super();

      this.people = [];
      this.showPersonForm = false;
      this.maxYearsInCompanyFilter = 0;
  }

  render() {
    return html`
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
      <h2 class="text-center">Personas</h2>
      <div class="row" id="peopleList">
          <div class="row row-cols-1 row-cols-sm-4">
              ${this.people.filter(
                  person => person.yearsInCompany <= this.maxYearsInCompanyFilter
              ).map(
                  person => html `<persona-ficha-listado 
                        fname="${person.name}"
                        yearsInCompany="${person.yearsInCompany}"
                        profile="${person.profile}"
                        .photo="${person.photo}"
                        @delete-person="${this.deletePerson}"
                        @info-person="${this.infoPerson}"
                        >
                      </persona-ficha-listado>`
              )}
          </div>
      </div> 
      <div class="row">
          <persona-form id="personForm" class="d-none" 
              @persona-form-close="${this.personFormClose}"
              @persona-form-store="${this.personFormStore}"
          >
          </persona-form>
      </div>
      <persona-main-dm @people-data-updated="${this.peopleDataUpdated}"></persona-main-dm>      
    `;
  }

  peopleDataUpdated(e) {
      console.log("peopleDataUpdated");

      this.people = e.detail.people;
  }

  updated(changedProperties) {
      console.log("updated");

      if (changedProperties.has("showPersonForm")) {
          console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

      if(this.showPersonForm === true) {
            this.showPersonFormData();
      } else {
            this.showPersonList();
      }
    }
     if (changedProperties.has("people")) {
          console.log("Ha cambiado el valor de la propiedad people en persona-main");
          console.log("xxx length en updated en persona-main " + this.people.length);
          this.dispatchEvent(new CustomEvent("updated-people", {
                detail: {
                  people: this.people
                }
          }));
      }

      if(changedProperties.has("maxYearsInCompanyFilter")) {
        console.log("Ha cambiado el valor de la propiedad maxYearsInCompanyFilter en persona-amin");
        console.log("Se van a mostrar las personas cuya antigüedad máxima sea "
                       + this.maxYearsInCompanyFilter + " años");
      }
  }

  personFormStore(e) {
      console.log("PersonFormStore");
      console.log("Se va a almacenar una persona");

      console.log(e);
      console.log(e.detail);

      if (e.detail.editingPerson === true) {
          console.log("Se va a actualizar la persona de nombre " + e.detail.person.name);

          this.people = this.people.map(
            person => person.name === e.detail.editingPerson
            ? person = e.detail.person : person
          )
      } else {
          console.log("se va a almacenar una persona nueva");
          this.people = [...this.people, e.detail.person];
      }

    console.log("Persona almacenada");

    this.showPersonForm = false;
}

  personFormClose() {
      console.log("PersonFormClose");
      console.log("Se ha cerrado el formulario de la persona");
      this.showPersonForm = false;
}

  showPersonFormData() {
      console.log("showPersonFormData");
      console.log("Mostrando el formulario de persona");
      this.shadowRoot.getElementById("peopleList").classList.add("d-none");
      this.shadowRoot.getElementById("personForm").classList.remove("d-none");
  }

  showPersonList() {
      console.log("showPersonLis");
      console.log("Mostrando el listado de personas");
      this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
      this.shadowRoot.getElementById("personForm").classList.add("d-none");
  }

  deletePerson(e) {
      console.log("deletePerson en persona main");
      console.log("se va a borrar la persona de nombre " + e.detail.name);

      this.people = this.people.filter(
          person => person.name != e.detail.name
    );
  }

  infoPerson(e) {
      console.log("infoPerson en persona-main");
      console.log("se ha pedido más información de la persona de nombre " + e.detail.name);

      let chosenPerson = this.people.filter(
          person => person.name === e.detail.name 
      );

      console.log("chosenPerson");

      this.shadowRoot.getElementById("personForm").person = chosenPerson[0];
      this.shadowRoot.getElementById("personForm").editingPerson = true;
      this.showPersonForm = true;
  }
}

customElements.define('persona-main', PersonaMain);