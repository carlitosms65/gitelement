import { LitElement, html } from 'lit-element';

class PersonaSidebar extends LitElement {

  static get properties() {
    return {
      peopleStats: {type: Object}
    }
  }

  constructor() {
    super();

    this.peopleStats = {};
  }

  render() {
    return html`
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
      <aside>  
        <section>
          <div>
              Hay <span class="badge badge-pill badge-primary">${this.peopleStats.numberOfPeople}</span> personas
          </div>
          <br />
          <label>Filtro años en la empresa</label>
          <div>
              <input type="range"  min="0" 
                  max="${this.peopleStats.maxYearsInCompany}" 
                  value="${this.peopleStats.maxYearsInCompany}" step="1" 
                  @input="${this.updateMaxYearsInCompanyFilter}"
              />
          </div>
          <br />
          <div class="mt-5">
            <button class="w-100 btn btn-success" style="font-size: 50px" @click="${this.newPerson}"><strong>+</strong></button>
          </div>
        </section>
      </aside>
    `;
  }

  updateMaxYearsInCompanyFilter(e) {
    console.log("updateMaxYearsInCompanyFilter");
    console.log("El rango vale " + e.target.value);

    this.dispatchEvent(new CustomEvent("update-max-years-filter", {
        detail: {
          maxYearsInCompany: e.target.value
        }
      }
    ));
  }

  newPerson(e) {
    console.log("newPerson en persona-sidebar");
    console.log("se va a crear una persona nueva");

    this.dispatchEvent(new CustomEvent("new-person", {}));
  }
}

customElements.define('persona-sidebar', PersonaSidebar);