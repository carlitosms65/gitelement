import { LitElement, html, css } from 'lit-element';
class TestBootstrap extends LitElement {

  static getStyles() {
    return css`
      .redbg {
        backgroumd-color: red;
      }
      .greenbg {
        backgroumd-color: green;
      }
      .bluebg {
        backgroumd-color: blue;
      }
      .greybg {
        backgroumd-color: grey;
      }
    `;
  }
  
  render() {
    return html`
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
     <h3>Test Bootstrap</h3>
     <div class="row greybg">
        <div class="col redbg">col 1</div>
        <div class="col greenbg">col 2</div>
        <div class="col bluebg">col 3</div>
     </div> 
    `;
  }
}

customElements.define('test-bootstrap', TestBootstrap);